﻿Imports OBrIM

Module TestRun
    Private WithEvents brim As OBrIMConn
    Private tests As List(Of ITest)

    Sub main()
        brim = New OBrIMConn(UIMode.SeparateWindow)
        OBrIMSettings.LogMessages = True
        tests = New List(Of ITest)
        tests.Add(New ObjectAssignment())
        tests.Add(New UnitConversion())
        tests.Add(New ColorOpacity())
        tests.Add(New CADD())
    End Sub

    Private Sub brim_MessageReceived(eventName As String, args As Object) Handles brim.MessageReceived
        System.Console.WriteLine("Event:" + eventName)

        If eventName = "AppLoaded" Then
            System.Console.WriteLine("User:" + brim.UserFirstName + " " + brim.UserLastName)
            brim.ProjectCreate("VB.NET Tests")
        End If

        For i As Integer = 0 To tests.Count - 1
            Try
                tests(i).BrIMEvent(brim, eventName, args)
            Catch e As Exception
                TestLogger.LogError(Nothing, e.ToString())
            End Try
        Next
    End Sub
End Module
