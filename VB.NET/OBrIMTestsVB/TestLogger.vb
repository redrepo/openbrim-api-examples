﻿Public Class TestLogger

    Public Shared Sub Log(test As ITest, msg As String)
        System.Console.WriteLine(msg)
    End Sub

    Public Shared Sub LogError(test As ITest, msg As String)
        Console.ForegroundColor = ConsoleColor.Red
        System.Console.WriteLine(msg)
        Console.ForegroundColor = ConsoleColor.White
    End Sub
End Class
