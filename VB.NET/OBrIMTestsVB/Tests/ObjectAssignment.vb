﻿Imports OBrIM
Imports OBrIMTestsVB

Public Class ObjectAssignment
    Implements ITest

    Public ReadOnly Property Name As String Implements ITest.Name
        Get
            Return "Parameter with Object Values"
        End Get
    End Property

    Private Sub BrIMEvent(brim As OBrIMConn, eventName As String, args As Object) Implements ITest.BrIMEvent

        If eventName = "ProjectCreate" Then
            Dim testPrj = brim.ObjectGet()

            Dim testObj = brim.ObjectCreate(testPrj.ID, "Type1", "Name1")
            brim.ParamCreate(testObj.ID, "Value", 10)

            brim.ParamCreate(testPrj.ID, "ObjRefA", testObj.Name, "Parameter refering to object by name", testObj.ObjType)
            brim.ParamCreate(testPrj.ID, "ObjRefB", testObj.ID, "Parameter refering to object by id", testObj.ObjType)

            brim.ParamCreate(testPrj.ID, "ValueOfRefA", "ObjRefA.Value")
            brim.ParamCreate(testPrj.ID, "ValueOfRefB", "ObjRefB.Value")

            Dim o = testPrj.ParamValueAsObject("ObjRefB")
            If o Is Nothing Then
                TestLogger.LogError(Me, "OBrIMObj.ParamValueAsObject() method failed and returned null")
            End If
        End If
    End Sub
End Class
