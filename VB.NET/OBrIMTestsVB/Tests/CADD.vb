﻿Imports OBrIM
Imports OBrIMTestsVB

Public Class CADD
    Implements ITest

    Private Shared WithEvents myTimer As New System.Windows.Forms.Timer()
    Private Shared brim As OBrIMConn

    Public ReadOnly Property Name As String Implements ITest.Name
        Get
            Return "CADD API"
        End Get
    End Property

    ' in 10 seconds we will display CADD User Select Window
    Private Shared Sub TimerEventProcessor(myObject As Object, ByVal myEventArgs As EventArgs) Handles myTimer.Tick
        myTimer.Stop()
        Dim resp = brim.CADDUserSelect()
    End Sub

    Private Sub BrIMEvent(brim As OBrIMConn, eventName As String, args As Object) Implements ITest.BrIMEvent
        CADD.brim = brim

        If eventName = "ProjectCreate" Then
            Dim testPrj = brim.ObjectGet()
            ' create an object that contains drawings
            brim.ObjectCreate(testPrj.ID, "PotBearingNonGuided", "CADDTest")
        End If

        If eventName = "ProjectCompileComplete" Then
            myTimer.Interval = 10000
            myTimer.Start()
            TestLogger.Log(Me, "In 10 seconds, wait for me to open CADD Select UI.")
        End If


        If eventName = "CADDSelected" Then
            Dim caddID = args.JSON("cadd")
            Dim resp = brim.CADDGet(caddID) ' the CADD object
            TestLogger.Log(Me, "You have selected " + caddID + ".")
        End If
    End Sub
End Class
