﻿Imports OBrIM
Imports OBrIMTestsVB

Public Class ColorOpacity
    Implements ITest

    Public ReadOnly Property Name As String Implements ITest.Name
        Get
            Return "Color & Opacity"
        End Get
    End Property

    Private Sub BrIMEvent(brim As OBrIMConn, eventName As String, args As Object) Implements ITest.BrIMEvent
        If eventName = "ProjectCreate" Then
            Dim testPrj = brim.ObjectGet()

            ' create 2 pier objects
            ' we will change the opacity and the color of the first pier object

            Dim pier1 = brim.ObjectCreate(testPrj.ID, "OBrIMPierHammerhead", "ColorTest1")

            Dim pier2 = brim.ObjectCreate(testPrj.ID, "OBrIMPierHammerhead", "ColorTest2")
            brim.ParamChangeValueNum(pier2.ID, "LocX", 1200)

            brim.ObjectSetOpacity(pier1.ID, 0.5)

            ' below are all equivalent (all sets red color)
            brim.ObjectSetColor(pier1.ID, 255, 0, 0)
            brim.ObjectSetColor(pier1.ID, System.Drawing.Color.Red)
            brim.ObjectSetColor(pier1.ID, "red")
            brim.ObjectSetColor(pier1.ID, "#FF0000")

            brim.GraphicsRefresh()
            brim.AppShow(AppViews.Graphics3D)

        End If
    End Sub
End Class
