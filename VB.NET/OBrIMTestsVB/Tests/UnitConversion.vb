﻿Imports OBrIM
Imports OBrIMTestsVB

Public Class UnitConversion
    Implements ITest

    Public ReadOnly Property Name As String Implements ITest.Name
        Get
            Return "Unit Conversion"
        End Get
    End Property

    Private Sub BrIMEvent(brim As OBrIMConn, eventName As String, args As Object) Implements ITest.BrIMEvent
        If eventName = "ProjectCreate" Then
            Dim testPrj = brim.ObjectGet()

            Dim internalUnit = brim.ObjectCreate(testPrj.ID, "Unit", "Internal")
            brim.ParamCreate(internalUnit.ID, "Length", "Inch")
            brim.ParamCreate(internalUnit.ID, "Force", "Kip")
            brim.ParamCreate(internalUnit.ID, "Angle", "Radian")
            brim.ParamCreate(internalUnit.ID, "Temperature", "Fahrenheit")

            Dim geometryUnit = brim.ObjectCreate(testPrj.ID, "Unit", "Geometry")
            brim.ParamCreate(geometryUnit.ID, "Length", "Feet")
            brim.ParamCreate(geometryUnit.ID, "Force", "Kip")
            brim.ParamCreate(geometryUnit.ID, "Angle", "Radian")
            brim.ParamCreate(geometryUnit.ID, "Temperature", "Fahrenheit")

            brim.ParamCreate(testPrj.ID, "UnitConversionTest", "1", "", "Expr", "Length", "Geometry")

            Dim value1 As Double = brim.UnitConvertToProject(testPrj.ID, "UnitConversionTest", 1)
            If Not value1 = 12 Then
                TestLogger.LogError(Me, "UnitConvertToProject() method failed and returned " + value1.ToString())
            End If

            Dim value2 As Double = brim.UnitConvertToUI(testPrj.ID, "UnitConversionTest", 12)
            If Not value2 = 1 Then
                TestLogger.LogError(Me, "UnitConvertToUI() method failed and returned " + value2.ToString())
            End If
        End If
    End Sub
End Class
