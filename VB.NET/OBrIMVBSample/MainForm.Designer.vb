﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.MainMenu = New System.Windows.Forms.MenuStrip()
        Me.UserMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.LogoutMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewGraphicsMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewCADDMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewGISMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewHistoryMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewTeamMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.MainHeader = New System.Windows.Forms.Panel()
        Me.listProjects = New System.Windows.Forms.ComboBox()
        Me.StatusPaneContainer = New System.Windows.Forms.SplitContainer()
        Me.MainContainer = New System.Windows.Forms.SplitContainer()
        Me.panelLibSelectBoxes = New System.Windows.Forms.TableLayoutPanel()
        Me.listCategories = New System.Windows.Forms.ComboBox()
        Me.listObjects = New System.Windows.Forms.ComboBox()
        Me.listAuthors = New System.Windows.Forms.ComboBox()
        Me.txtStatus = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblStatusHeader = New System.Windows.Forms.Label()
        Me.spsheet = New OBrIM.Spreadsheet()
        Me.MainMenu.SuspendLayout()
        Me.MainHeader.SuspendLayout()
        CType(Me.StatusPaneContainer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusPaneContainer.Panel1.SuspendLayout()
        Me.StatusPaneContainer.Panel2.SuspendLayout()
        Me.StatusPaneContainer.SuspendLayout()
        CType(Me.MainContainer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MainContainer.Panel1.SuspendLayout()
        Me.MainContainer.SuspendLayout()
        Me.panelLibSelectBoxes.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainMenu
        '
        Me.MainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UserMenu, Me.ViewToolStripMenuItem})
        Me.MainMenu.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu.Name = "MainMenu"
        Me.MainMenu.Size = New System.Drawing.Size(1050, 24)
        Me.MainMenu.TabIndex = 0
        Me.MainMenu.Text = "MenuStrip1"
        '
        'UserMenu
        '
        Me.UserMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LogoutMenu})
        Me.UserMenu.Name = "UserMenu"
        Me.UserMenu.Size = New System.Drawing.Size(42, 20)
        Me.UserMenu.Text = "User"
        '
        'LogoutMenu
        '
        Me.LogoutMenu.Name = "LogoutMenu"
        Me.LogoutMenu.Size = New System.Drawing.Size(112, 22)
        Me.LogoutMenu.Text = "Logout"
        '
        'ViewToolStripMenuItem
        '
        Me.ViewToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ViewGraphicsMenu, Me.ViewCADDMenu, Me.ViewGISMenu, Me.ViewHistoryMenu, Me.ViewTeamMenu})
        Me.ViewToolStripMenuItem.Name = "ViewToolStripMenuItem"
        Me.ViewToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.ViewToolStripMenuItem.Text = "View"
        '
        'ViewGraphicsMenu
        '
        Me.ViewGraphicsMenu.Name = "ViewGraphicsMenu"
        Me.ViewGraphicsMenu.Size = New System.Drawing.Size(140, 22)
        Me.ViewGraphicsMenu.Text = "3D Model"
        '
        'ViewCADDMenu
        '
        Me.ViewCADDMenu.Name = "ViewCADDMenu"
        Me.ViewCADDMenu.Size = New System.Drawing.Size(140, 22)
        Me.ViewCADDMenu.Text = "2D Drawings"
        '
        'ViewGISMenu
        '
        Me.ViewGISMenu.Name = "ViewGISMenu"
        Me.ViewGISMenu.Size = New System.Drawing.Size(140, 22)
        Me.ViewGISMenu.Text = "GIS"
        '
        'ViewHistoryMenu
        '
        Me.ViewHistoryMenu.Name = "ViewHistoryMenu"
        Me.ViewHistoryMenu.Size = New System.Drawing.Size(140, 22)
        Me.ViewHistoryMenu.Text = "History"
        '
        'ViewTeamMenu
        '
        Me.ViewTeamMenu.Name = "ViewTeamMenu"
        Me.ViewTeamMenu.Size = New System.Drawing.Size(140, 22)
        Me.ViewTeamMenu.Text = "Team"
        '
        'MainHeader
        '
        Me.MainHeader.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.MainHeader.Controls.Add(Me.listProjects)
        Me.MainHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.MainHeader.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.MainHeader.Location = New System.Drawing.Point(0, 24)
        Me.MainHeader.Name = "MainHeader"
        Me.MainHeader.Size = New System.Drawing.Size(1050, 43)
        Me.MainHeader.TabIndex = 1
        '
        'listProjects
        '
        Me.listProjects.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.listProjects.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.listProjects.FormattingEnabled = True
        Me.listProjects.Location = New System.Drawing.Point(12, 8)
        Me.listProjects.Name = "listProjects"
        Me.listProjects.Size = New System.Drawing.Size(310, 28)
        Me.listProjects.Sorted = True
        Me.listProjects.TabIndex = 0
        '
        'StatusPaneContainer
        '
        Me.StatusPaneContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.StatusPaneContainer.Location = New System.Drawing.Point(0, 67)
        Me.StatusPaneContainer.Name = "StatusPaneContainer"
        Me.StatusPaneContainer.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'StatusPaneContainer.Panel1
        '
        Me.StatusPaneContainer.Panel1.Controls.Add(Me.MainContainer)
        '
        'StatusPaneContainer.Panel2
        '
        Me.StatusPaneContainer.Panel2.Controls.Add(Me.txtStatus)
        Me.StatusPaneContainer.Panel2.Controls.Add(Me.Panel1)
        Me.StatusPaneContainer.Size = New System.Drawing.Size(1050, 473)
        Me.StatusPaneContainer.SplitterDistance = 350
        Me.StatusPaneContainer.TabIndex = 2
        '
        'MainContainer
        '
        Me.MainContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainContainer.Location = New System.Drawing.Point(0, 0)
        Me.MainContainer.Name = "MainContainer"
        '
        'MainContainer.Panel1
        '
        Me.MainContainer.Panel1.Controls.Add(Me.spsheet)
        Me.MainContainer.Panel1.Controls.Add(Me.panelLibSelectBoxes)
        Me.MainContainer.Size = New System.Drawing.Size(1050, 350)
        Me.MainContainer.SplitterDistance = 510
        Me.MainContainer.TabIndex = 3
        '
        'panelLibSelectBoxes
        '
        Me.panelLibSelectBoxes.ColumnCount = 3
        Me.panelLibSelectBoxes.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.0411!))
        Me.panelLibSelectBoxes.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.9589!))
        Me.panelLibSelectBoxes.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 161.0!))
        Me.panelLibSelectBoxes.Controls.Add(Me.listCategories, 2, 0)
        Me.panelLibSelectBoxes.Controls.Add(Me.listObjects, 1, 0)
        Me.panelLibSelectBoxes.Controls.Add(Me.listAuthors, 0, 0)
        Me.panelLibSelectBoxes.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelLibSelectBoxes.Location = New System.Drawing.Point(0, 0)
        Me.panelLibSelectBoxes.Name = "panelLibSelectBoxes"
        Me.panelLibSelectBoxes.RowCount = 1
        Me.panelLibSelectBoxes.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.panelLibSelectBoxes.Size = New System.Drawing.Size(510, 29)
        Me.panelLibSelectBoxes.TabIndex = 0
        '
        'listCategories
        '
        Me.listCategories.Dock = System.Windows.Forms.DockStyle.Fill
        Me.listCategories.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.listCategories.FormattingEnabled = True
        Me.listCategories.Location = New System.Drawing.Point(351, 3)
        Me.listCategories.Name = "listCategories"
        Me.listCategories.Size = New System.Drawing.Size(156, 21)
        Me.listCategories.TabIndex = 2
        '
        'listObjects
        '
        Me.listObjects.Dock = System.Windows.Forms.DockStyle.Fill
        Me.listObjects.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.listObjects.FormattingEnabled = True
        Me.listObjects.Location = New System.Drawing.Point(174, 3)
        Me.listObjects.Name = "listObjects"
        Me.listObjects.Size = New System.Drawing.Size(171, 21)
        Me.listObjects.TabIndex = 1
        '
        'listAuthors
        '
        Me.listAuthors.Dock = System.Windows.Forms.DockStyle.Fill
        Me.listAuthors.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.listAuthors.FormattingEnabled = True
        Me.listAuthors.Location = New System.Drawing.Point(3, 3)
        Me.listAuthors.Name = "listAuthors"
        Me.listAuthors.Size = New System.Drawing.Size(165, 21)
        Me.listAuthors.TabIndex = 0
        '
        'txtStatus
        '
        Me.txtStatus.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtStatus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtStatus.Location = New System.Drawing.Point(0, 21)
        Me.txtStatus.Multiline = True
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.Size = New System.Drawing.Size(1050, 98)
        Me.txtStatus.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblStatusHeader)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1050, 21)
        Me.Panel1.TabIndex = 0
        '
        'lblStatusHeader
        '
        Me.lblStatusHeader.AutoSize = True
        Me.lblStatusHeader.Location = New System.Drawing.Point(6, 4)
        Me.lblStatusHeader.Name = "lblStatusHeader"
        Me.lblStatusHeader.Size = New System.Drawing.Size(88, 13)
        Me.lblStatusHeader.TabIndex = 0
        Me.lblStatusHeader.Text = "Status Messages"
        '
        'spsheet
        '
        Me.spsheet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.spsheet.Location = New System.Drawing.Point(0, 29)
        Me.spsheet.Name = "spsheet"
        Me.spsheet.Size = New System.Drawing.Size(510, 321)
        Me.spsheet.TabIndex = 1
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1050, 540)
        Me.Controls.Add(Me.StatusPaneContainer)
        Me.Controls.Add(Me.MainHeader)
        Me.Controls.Add(Me.MainMenu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MainMenu
        Me.Name = "MainForm"
        Me.Text = "OpenBrIM VB.NET Sample..."
        Me.MainMenu.ResumeLayout(False)
        Me.MainMenu.PerformLayout()
        Me.MainHeader.ResumeLayout(False)
        Me.StatusPaneContainer.Panel1.ResumeLayout(False)
        Me.StatusPaneContainer.Panel2.ResumeLayout(False)
        Me.StatusPaneContainer.Panel2.PerformLayout()
        CType(Me.StatusPaneContainer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusPaneContainer.ResumeLayout(False)
        Me.MainContainer.Panel1.ResumeLayout(False)
        CType(Me.MainContainer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MainContainer.ResumeLayout(False)
        Me.panelLibSelectBoxes.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MainMenu As MenuStrip
    Friend WithEvents MainHeader As Panel
    Friend WithEvents listProjects As ComboBox
    Friend WithEvents StatusPaneContainer As SplitContainer
    Friend WithEvents MainContainer As SplitContainer
    Friend WithEvents txtStatus As TextBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents lblStatusHeader As Label
    Friend WithEvents panelLibSelectBoxes As TableLayoutPanel
    Friend WithEvents listCategories As ComboBox
    Friend WithEvents listObjects As ComboBox
    Friend WithEvents listAuthors As ComboBox
    Friend WithEvents UserMenu As ToolStripMenuItem
    Friend WithEvents LogoutMenu As ToolStripMenuItem
    Friend WithEvents ViewToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ViewGraphicsMenu As ToolStripMenuItem
    Friend WithEvents ViewCADDMenu As ToolStripMenuItem
    Friend WithEvents ViewGISMenu As ToolStripMenuItem
    Friend WithEvents ViewHistoryMenu As ToolStripMenuItem
    Friend WithEvents ViewTeamMenu As ToolStripMenuItem
    Friend WithEvents spsheet As OBrIM.Spreadsheet
End Class
