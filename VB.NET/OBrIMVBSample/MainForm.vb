﻿Imports OBrIM

Public Class MainForm
    Private WithEvents brimConn As OBrIMConn

    Private ActiveObj As OBrIMObj
    Private ActiveParams As List(Of OBrIMParam)
    Private ActiveSpreadsheet As SpreadsheetManager

    Public Sub New()
        InitializeComponent()

        ' initialize openbrim
        brimConn = New OBrIMConn(UIMode.EmbeddedControl)
        OBrIMSettings.LogMessages = True

        ' add OpenBrIM window to our form
        MainContainer.Panel2.Controls.Add(brimConn.GetUserControl())

        ' project list should inform the user that we started the load process
        listProjects.Items.Add("Loading...")
        listProjects.SelectedItem = listProjects.Items(0)

        spsheet.Visible = False
        panelLibSelectBoxes.Visible = False

    End Sub

    ' we receive all messages from OpenBrIM through this event
    Private Sub brimConn_MessageReceived(eventName As String, args As Object) Handles brimConn.MessageReceived
        If eventName = "AppLoaded" Then
            ' OpenBrIM says that user is logged in successfully
            ' let's request projects of the user
            brimConn.ProjectGetAll()
        ElseIf eventName = "ProjectOpened" Then
            LoadLibraryObjects()
            brimConn.AppShow(AppViews.Graphics3D)
            spsheet.Visible = True
            panelLibSelectBoxes.Visible = True
        ElseIf eventName = "ProjectList" Then
            LoadProjects(args) ' we received the projects of the user, populate UI
        End If

        txtStatus.AppendText("OpenBrIM: " + eventName + vbNewLine)
        txtStatus.SelectionStart = txtStatus.Text.Length
    End Sub

    Private Sub LoadProjects(resp As OBrIMResponse)
        listProjects.Items.Clear()
        If resp.Projects.Count > 0 Then
            For i As Integer = 0 To resp.Projects.Count - 1
                listProjects.Items.Add(New ListItem(resp.Projects(i).ID, resp.Projects(i).Name))
            Next
        Else
            listProjects.Items.Add("No Project Found")
        End If

        listProjects.Items.Add(New ListItem("NewProject", "Create New Project..."))
        listProjects.SelectedItem = listProjects.Items(0)
    End Sub

    Private Library As OBrIMObjList
    Private Sub LoadLibraryObjects()
        Dim resp = brimConn.LibObjList()
        Library = resp.ObjData.Objects

        Dim selItem As ListItem = Nothing

        listAuthors.Items.Clear()
        For i As Integer = 0 To Library.Count - 1
            Dim author = Library.Get(i)
            Dim item = New ListItem(author.ID, author.Name)
            listAuthors.Items.Add(item)
            If item.Label = "OpenBrIM Bridge Objects" Then
                selItem = item
            End If
        Next

        If Not selItem Is Nothing Then
            listAuthors.SelectedItem = selItem
        End If
    End Sub

    Private Sub listProjects_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listProjects.SelectedIndexChanged
        If Not TypeOf listProjects.SelectedItem Is ListItem Then Return

        Dim pi As ListItem = listProjects.SelectedItem
        If pi.Key = "NewProject" Then
            Dim prjName = InputBox("What is the name of the project?", "Project Name", "New Project")
            Dim id = brimConn.ProjectCreate(prjName)
            listProjects.Items.Add(New ListItem(id, prjName))
        Else
            brimConn.ProjectOpen(pi.Key)
        End If
    End Sub

    Private Sub listAuthors_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listAuthors.SelectedIndexChanged
        Dim aListItem As ListItem = listAuthors.SelectedItem
        Dim author = Library.GetByID(aListItem.Key)
        listObjects.Items.Clear()
        For i = 0 To author.Objects.Count - 1
            Dim item = author.Objects.Get(i)
            Dim name As String = item.GetParam("ObjLabel").Expr
            listObjects.Items.Add(New ListItem(item.ID, name))
        Next
        listObjects.SelectedItem = listObjects.Items(0)
    End Sub

    Private Sub listObjects_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listObjects.SelectedIndexChanged
        Dim aListItem As ListItem = listAuthors.SelectedItem
        Dim author = Library.GetByID(aListItem.Key)

        Dim oListItem As ListItem = listObjects.SelectedItem
        ActiveObj = author.Objects.GetByID(oListItem.Key)

        Dim cats = New Dictionary(Of String, String)()

        Dim ig = ActiveObj.Objects.Get(0)
        For i = 0 To ig.Params.Count - 1
            If ig.Params.Get(i).Name = "N" Then Continue For
            Dim cat As String = ig.Params.Get(i).DisplayCategory
            If cat Is Nothing Then cat = "Default"
            If Not cats.ContainsKey(cat) Then
                cats.Add(cat, cat)
            End If
        Next

        listCategories.Items.Clear()
        For Each key As KeyValuePair(Of String, String) In cats
            listCategories.Items.Add(key.Key)
        Next
        listCategories.SelectedItem = listCategories.Items(0)
    End Sub

    Private Sub listCategories_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listCategories.SelectedIndexChanged
        Dim selcat As String = listCategories.SelectedItem

        Dim params As New List(Of OBrIMParam)

        Dim ig = ActiveObj.Objects.Get(0)
        params.Add(ActiveObj.GetParam("N"))
        For i = 0 To ig.Params.Count - 1
            Dim cat As String = ig.Params.Get(i).DisplayCategory
            If cat Is Nothing Then cat = "Default"
            If cat = selcat Then
                params.Add(ig.Params.Get(i))
            End If
        Next

        ActiveParams = params
        ActiveSpreadsheet = New SpreadsheetManager(brimConn, spsheet, ActiveObj, ActiveParams)
        spsheet.Refresh()
    End Sub

    Private Sub spsheet_SetData(sender As Object, e As SpreadsheetEventArgs)
        ActiveSpreadsheet.SetData(sender, e)
    End Sub

    Private Sub spsheet_GetData(sender As Object, e As SpreadsheetEventArgs)
        ActiveSpreadsheet.GetData(sender, e)
    End Sub

    Private Sub spsheet_ActivateCellAction(sender As Object, e As SpreadsheetEventActionActivateArgs)
        ActiveSpreadsheet.ActivateCellAction(sender, e)
    End Sub

    Private Sub spsheet_GetCellActions(sender As Object, e As SpreadsheetEventActionArgs)
        ActiveSpreadsheet.GetCellActions(sender, e)
    End Sub

    Private Sub LogoutMenu_Click(sender As Object, e As EventArgs) Handles LogoutMenu.Click
        brimConn.UserLogout()
    End Sub

    Private Sub ViewGraphicsMenu_Click(sender As Object, e As EventArgs) Handles ViewGraphicsMenu.Click
        brimConn.AppShow(AppViews.Graphics3D)
    End Sub

    Private Sub ViewCADDMenu_Click(sender As Object, e As EventArgs) Handles ViewCADDMenu.Click
        brimConn.AppShow(AppViews.CADD)
    End Sub

    Private Sub ViewGISMenu_Click(sender As Object, e As EventArgs) Handles ViewGISMenu.Click
        brimConn.AppShow(AppViews.GIS)
    End Sub

    Private Sub ViewHistoryMenu_Click(sender As Object, e As EventArgs) Handles ViewHistoryMenu.Click
        brimConn.AppShow(AppViews.Revisions)
    End Sub

    Private Sub ViewTeamMenu_Click(sender As Object, e As EventArgs) Handles ViewTeamMenu.Click
        brimConn.AppShow(AppViews.Team)
    End Sub
End Class

Public Class ListItem
    Public Key As String
    Public Label As String

    Public Sub New(key As String, label As String)
        Me.Key = key
        Me.Label = label
    End Sub

    Public Overrides Function ToString() As String
        Return Me.Label
    End Function
End Class
