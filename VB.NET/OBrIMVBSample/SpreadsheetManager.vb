﻿Imports OBrIM

Public Class SpreadsheetManager
    Private BrIM As OBrIMConn
    Private Sheet As Spreadsheet
    Private Obj As OBrIMObj
    Private Parameters As List(Of OBrIMParam)

    Public Sub New(conn As OBrIMConn, sp As Spreadsheet, o As OBrIM.OBrIMObj, params As List(Of OBrIMParam))
        BrIM = conn
        Sheet = sp
        Obj = o
        Parameters = params
        Sheet.Refresh()
    End Sub

    Public Sub GetData(sender As Object, e As SpreadsheetEventArgs)
        Dim prj As OBrIMObj = BrIM.ObjectGet()
        Dim objs As List(Of OBrIMObj) = prj.SearchObjectsByType(Obj.Name)

        e.Data.SetColRowCount(Parameters.Count, objs.Count)

        ' cell values
        For row = e.RowFrom To e.RowTo
            For col = e.ColFrom To e.ColTo

                Dim o As OBrIMObj = Nothing
                If row > 0 And row <= objs.Count Then
                    o = objs(row - 1)
                End If

                Dim p As OBrIMParam = Nothing
                If col > 0 And col < Parameters.Count Then
                    p = Parameters(col - 1)
                    If Not o Is Nothing Then
                        p = o.SearchParam(p.Name)
                    End If
                End If

                If Not p Is Nothing And Not o Is Nothing Then
                    e.Data.SetValue(row, col, p.Expr, p.Expr)
                Else
                    e.Data.SetValue(0, col, "", "")
                End If
            Next
        Next

        ' column headers
        For col = e.ColFrom To e.ColTo
            If col > 0 And col <= Parameters.Count Then
                Dim p As OBrIMParam = Parameters(col - 1)
                Dim label As String = p.Desc
                If label Is Nothing Or label = "" Then
                    label = p.Name
                End If
                e.Data.SetValue(0, col, label, label)
            Else
                e.Data.SetValue(0, col, "", "")
            End If
        Next
    End Sub

    Public Sub SetData(sender As Object, e As SpreadsheetEventArgs)
        Try
            Dim prj As OBrIMObj = BrIM.ObjectGet()
            Dim objs As List(Of OBrIMObj) = prj.SearchObjectsByType(Obj.Name)

            For Each i As SpreadsheetDataItem In e.Data.Values.Values
                Dim row = i.Row
                Dim col = i.Col

                If col < 1 Or col > Parameters.Count Then Continue For

                Dim o As OBrIMObj = Nothing
                If row > 0 And row <= objs.Count Then
                    o = objs(row - 1)
                End If

                Dim p As OBrIMParam = Parameters(col - 1)
                If o Is Nothing Then
                    o = BrIM.ObjectCreate(prj.ID, Obj.Name)
                End If

                Dim value = i.EditValue
                BrIM.ParamChangeValueExpr(o.ID, p.Name, value)
            Next

            Sheet.Refresh()
        Catch ex As Exception
            OBrIMLogger.Error(ex.ToString())
        End Try
    End Sub

    Public Sub ActivateCellAction(sender As Object, e As SpreadsheetEventActionActivateArgs)
        Dim prj As OBrIMObj = BrIM.ObjectGet()
        Dim objs As List(Of OBrIMObj) = prj.SearchObjectsByType(Obj.Name)

        If e.ID = "deleterows" Then
            For i As Integer = Math.Max(1, e.RowFrom) To Math.Min(objs.Count, e.RowTo)
                BrIM.ObjectDelete(objs(i - 1).ID)
            Next
        End If
    End Sub

    Public Sub GetCellActions(sender As Object, e As SpreadsheetEventActionArgs)
        Dim prj As OBrIMObj = BrIM.ObjectGet()
        Dim objs As List(Of OBrIMObj) = prj.SearchObjectsByType(Obj.Name)

        If (e.RowFrom <= objs.Count) Then
            e.Actions.Add(New SpreadsheetActionItem("deleterows", "Delete Row(s)"))
        End If
    End Sub

End Class
