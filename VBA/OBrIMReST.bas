Attribute VB_Name = "OBrIMReST"
Option Explicit

' ===========================================================================
' Module OBrIMReST. Accessing OpenBrIM Data directly through openbrim.org backend
' Created by Ali Koc, modified by Vlasov Alexey
' Last modify: 29.01.17


' Global variables:
Private userEmail As String
Private userPassword As String
Private userFirstName As String
Private userLastName As String
Private userOrganization As String

Private Const DEL = "._DEL_."

Public Type BrIMProject
    id As String
    name As String
    modified As Date
End Type

' Try to login to openbrim.org server
' Return result: True (Success login) of False (don't login)
Public Function OBrIMLogin(email As String, password As String) As Boolean
    Dim resp As String: resp = RemoteRequest("action", "login", "email", email, "password", password)
    Dim data() As String: data = Split(resp, DEL)
    OBrIMLogin = (data(0) = "OK") ' result value
    If OBrIMLogin Then
        userEmail = data(1)   'save to global variables
        userPassword = data(2)
        userFirstName = data(3)
        userLastName = data(4)
        userOrganization = data(5)
    End If
End Function

Public Sub OBrIMLogout()
    userPassword = ""
End Sub

' Return list of all current user projects (as an array of OBrIMProject objects)
Public Function OBrIMGetProjects() As BrIMProject()
    Dim resp As String: resp = RemoteRequest("action", "list")
    Dim projects() As String: projects = Split(resp, DEL)
    
    ' up to 1000 projects per user
    Dim projectList() As BrIMProject: ReDim projectList(0 To 1000)
    
    Dim count As Long: count = 0
    
    Dim i As Long
    For i = 1 To UBound(projects)
        If Not projects(i) = "" Then
            Dim prj() As String: prj = Split(projects(i), ".__.")
            projectList(count).id = prj(0)
            projectList(count).name = prj(1)
            projectList(count).modified = DateAdd("s", CDbl(prj(2)) / 1000, "01/01/1970 00:00:00")
            count = count + 1
        End If
    Next
    
    ReDim Preserve projectList(0 To count - 1)
    
    OBrIMGetProjects = projectList
End Function

' Load project by id and return project data (XML string)
Public Function OBrIMLoadProject(id As String) As String
    Dim resp As String: resp = RemoteRequest("action", "get", "project", id)
    Dim respA() As String: respA = Split(resp, DEL)
    Dim projectName As String: projectName = respA(2)
    Dim projectdata As String: projectdata = respA(3)
    OBrIMLoadProject = projectdata
End Function

Public Sub OBrIMSaveProject(id As String, projectName As String, data As String)
    Dim resp As String: resp = RemoteRequest("action", "save", "project", id, "data", data, "projectname", projectName)
End Sub

Public Sub OBrIMDeleteProject(id As String)
    Dim resp As String: resp = RemoteRequest("action", "delete", "project", id)
End Sub


' Execute request to OBrIM.red server
' Parameters: set of pair (key: string, value: string)
Private Function RemoteRequest(ParamArray args()) As String
    Dim qstring As String: qstring = "?"
    
    Dim email As String: email = ""
    Dim password As String: password = ""
    
    Dim i As Long
    For i = 0 To UBound(args) Step 2
        Dim key As String: key = args(i)
        Dim value As String: value = args(i + 1)
        qstring = qstring + key + "=" + URLEncode(value) + "&"
        If key = "email" Then email = URLEncode(value)
        If key = "password" Then password = URLEncode(value)
    Next
    
    If email = "" Then
        If userEmail = "" Then Call Err.Raise(5000, , "You need to login first!")
        qstring = qstring + "email" + "=" + URLEncode(userEmail) + "&"
    End If
    
    If password = "" Then
        If userPassword = "" Then Call Err.Raise(5001, , "You need to login first!")
        qstring = qstring + "password" + "=" + URLEncode(userPassword) + "&"
    End If
    
    qstring = qstring + "random" + "=" & Rnd


    Dim pXmlHttp As Object
    Set pXmlHttp = CreateObject("MSXML2.XMLHTTP")
    Dim strOpen As String: strOpen = "https://openbrim.org/connect" & qstring
    Call pXmlHttp.Open("GET", strOpen, False)
    Call pXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
    Call pXmlHttp.send(qstring)
    
    RemoteRequest = pXmlHttp.responseText
End Function


Private Function URLEncode(StringVal As String, Optional SpaceAsPlus As Boolean = False) As String

  Dim StringLen As Long: StringLen = Len(StringVal)

  If StringLen > 0 Then
    ReDim result(StringLen) As String
    Dim i As Long, CharCode As Integer
    Dim Char As String, Space As String

    If SpaceAsPlus Then Space = "+" Else Space = "%20"

    For i = 1 To StringLen
      Char = Mid$(StringVal, i, 1)
      CharCode = Asc(Char)
      Select Case CharCode
        Case 97 To 122, 65 To 90, 48 To 57, 45, 46, 95, 126
          result(i) = Char
        Case 32
          result(i) = Space
        Case 0 To 15
          result(i) = "%0" & Hex(CharCode)
        Case Else
          result(i) = "%" & Hex(CharCode)
      End Select
    Next i
    URLEncode = Join(result, "")
  End If
End Function

Public Function OBrIMProjectIdByName(name As String) As String
    Dim i As Long
    Dim projects() As BrIMProject: projects = OBrIMGetProjects
    Dim res As String: res = ""
    For i = 0 To UBound(projects)
        If projects(i).name = name Then res = projects(i).id
    Next
    OBrIMProjectIdByName = res
End Function

Public Function OBrIMProjectNameById(id As String) As String
    Dim i As Long
    Dim projects() As BrIMProject: projects = OBrIMGetProjects
    Dim res As String: res = ""
    For i = 0 To UBound(projects)
        If projects(i).id = id Then res = projects(i).name
    Next
    OBrIMProjectNameById = res
End Function

Public Sub OBrIMSaveProjectByName(projectName As String, data As String)
    Call OBrIMSaveProject(OBrIMProjectIdByName(projectName), projectName, data)
End Sub


