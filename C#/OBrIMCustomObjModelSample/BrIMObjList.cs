﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OBrIM;

namespace OBrIMCustomObjModelSample
{
    public class BrIMObjList<T> : IList<T>
    {
        private OBrIMObj brim;

        public BrIMObjList(OBrIMObj o) { this.brim = o; }

        public T this[int index]
        {
            get { return (T) Activator.CreateInstance(typeof(T), new object[] { brim.Objects.Get(index) }); }
            set { throw new NotImplementedException(); }
        }

        public int Count { get { return brim.Objects.Count; } }

        public bool IsReadOnly { get { return true; } }

        public T New(string name = null)
        {
            var no = Application.conn.ObjectCreate(brim.ID, typeof(T).Name, name);
            return (T)Activator.CreateInstance(typeof(T), new object[] { no });
        }
        
        public void Clear()
        {
            for(var i= this.brim.Objects.Count - 1; i > 0 ; i--)
            {
                this.brim.Objects.Remove(brim.Objects.Get(i));
            }
        }

        public bool Contains(T item)
        {
            BrIMObj search = (BrIMObj)(object)item;
            for (var i=0; i < this.brim.Objects.Count; i++)
            {
                if (this.brim.Objects.Get(i) == search.brim)
                {
                    return true;
                }
            }
            return false;
        }
        
        public int IndexOf(T item)
        {
            BrIMObj search = (BrIMObj)(object)item;
            for (var i = 0; i < this.brim.Objects.Count; i++)
            {
                if (this.brim.Objects.Get(i) == search.brim)
                {
                    return i;
                }
            }
            return -1;
        }
        
        public bool Remove(T item)
        {
            BrIMObj search = (BrIMObj)(object)item;
            this.brim.Objects.Remove(search.brim);
            return true;
        }

        public void RemoveAt(int index)
        {
            this.brim.Objects.Remove(this.brim.Objects.Get(index));
        }

        public void Insert(int index, T item)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public void Add(T item)
        {
            throw new MethodAccessException("Use New() method to create a new instance of object " + typeof(T).Name);
        }
    }
}
