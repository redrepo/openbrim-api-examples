﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OBrIM;

namespace OBrIMCustomObjModelSample
{
    public class Building : BrIMObj
    {
        public Building(OBrIMObj o) : base(o) { }

        public BrIMObjList<Story> Stories
        {
            get
            {
                OBrIMObj storyGroup;
                var stories = this.brim.SearchObjectByTypeAndNameDeep("Group", "Stories");
                if (stories.Count == 0)
                {
                    storyGroup = Application.conn.ObjectCreate(this.brim.ID, "Group", "Stories");
                }
                else
                {
                    storyGroup = stories[0];
                }
                return new BrIMObjList<Story>(storyGroup);
            }
        }
    }
}
