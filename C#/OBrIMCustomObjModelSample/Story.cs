﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OBrIM;

namespace OBrIMCustomObjModelSample
{
    public class Story : BrIMObj
    {
        public Story(OBrIMObj o) : base(o) { }

        public BrIMObjList<Slab> Slabs
        {
            get
            {
                OBrIMObj slabGroup;
                var slabs = this.brim.SearchObjectByTypeAndNameDeep("Group", "Slabs");
                if (slabs.Count == 0)
                {
                    slabGroup = Application.conn.ObjectCreate(this.brim.ID, "Group", "Slabs");
                }
                else
                {
                    slabGroup = slabs[0];
                }
                return new BrIMObjList<Slab>(slabGroup);
            }
        }

        public BrIMObjList<Column> Columns
        {
            get
            {
                OBrIMObj slabGroup;
                var slabs = this.brim.SearchObjectByTypeAndNameDeep("Group", "Slabs");
                if (slabs.Count == 0)
                {
                    slabGroup = Application.conn.ObjectCreate(this.brim.ID, "Group", "Slabs");
                }
                else
                {
                    slabGroup = slabs[0];
                }
                return new BrIMObjList<Column>(slabGroup);
            }
        }
    }
}
