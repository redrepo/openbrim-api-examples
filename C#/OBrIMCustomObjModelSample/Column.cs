﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OBrIM;

namespace OBrIMCustomObjModelSample
{
    public class Column: BrIMObj
    {
        public Column(OBrIMObj o): base(o) {
            // assign default values
            this.Width = 12;
            this.Depth = 12;
            this.Length = 96;
        }

        public double Width { get { return brim.ParamValueAsNumber("Width"); } set { brim.SetParamValueNum("Width", value); } }
        public double Depth { get { return brim.ParamValueAsNumber("Depth"); } set { brim.SetParamValueNum("Depth", value); } }
        public double Length { get { return brim.ParamValueAsNumber("Length"); } set { brim.SetParamValueNum("Length", value); } }
    }
}
