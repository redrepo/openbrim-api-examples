﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OBrIM;

namespace TestApp
{
    public partial class TestForm : Form
    {
        OBrIMConn OpenBrIM;
        OBrIMObj ActiveProject;
        OBrIMObj ActiveObject;
        OBrIMParam ActiveParameter;
        
        public TestForm()
        {
            InitializeComponent();
            OpenBrIM = new OBrIMConn(UIMode.SeparateWindow, true);
            OpenBrIM.MessageReceived += MessageReceived; // register for receiving messages from server
            UserLogInEvent(false);
        }

        private void TestForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            OpenBrIM.Exit(); // let OpenBrIM clean up after itself
        }

        void MessageReceived(string eventName, object arg)
        {
            AppendStatusLog(eventName);

            switch(eventName)
            {
                case "AppLoaded":           // OpenBrIM server has been connected and we are ready for operation. No operation can be performed before we receive this message.
                    break;
                case "AppClients":          // received the list of clients connected
                    List<OBrIMResponseApp> connectedApps = ((OBrIMResponse)arg).Apps;
                    for (int i = 0; i < connectedApps.Count; i++)
                        AppendStatusLog("Connection " + i.ToString() + " running at " + connectedApps[i].Label + ".");
                    AppendStatusLog("There are " + connectedApps.Count.ToString() + " active connections.");
                    break;
                case "ProjectList":         // received the list of projects of the logged in user as response to OpenBrIM.ProjectGetAll() call
                    List<OBrIMResponseProject> userProjects = ((OBrIMResponse)arg).Projects;
                    this.PopulateProjectList(userProjects);
                    AppendStatusLog("User has " + userProjects.Count.ToString() + " projects.");
                    break;
                case "ProjectCreate":       // received the information related to the newly created project as response to OpenBrIM.ProjectCreate() call.                    
                    OBrIMResponseProject newProject = ((OBrIMResponse)arg).Projects[0];
                    AppendStatusLog("Project " + newProject.Name + " with ID " + newProject.ID + " was created successfully.");
                    listProjects.Items.Add(new ListItem(newProject.Name, newProject.ID));
                    listProjects.SelectedIndex = listProjects.Items.Count - 1;
                    break;
                case "UserLoginSuccess":    // user logged in successfully
                    OpenBrIM.ProjectGetAll();
                    UserLogInEvent(true);
                    break;
                case "UserLogoutSuccess":   // user logged out successfully
                    UserLogInEvent(false);
                    break;
                case "Error":               // OpenBrIM encountered an error
                    string errorMessage = (string) arg;
                    break;
                case "ProjectClosing":      // Closing the currently open project
                    string closedProjectID = (string)arg;
                    break;
                case "ProjectOpened":       // openning the requested project was successfull
                    string openedProjectID = (string)arg;
                    break;
                case "DataHasChanged":      // the project has been changed and require a full compile before we can access the data
                    break;
                case "ProjectCompileStart": // project compilation has started, we cannot access the project data at this time
                    break;
                case "ProjectReceivedCompileOutput": // project compilation has ended, OpenBrIM modules are processing the results, we still cannot access the project data.
                    break;
                case "ProjectCompileFailed": // project compilation has failed. There must be some error with the project data
                    break;
                case "ProjectCompileComplete": // project comilation is done, we can access project data now.
                    // populate the project tree with the objects and parameters of the project
                    this.ActiveProject = OpenBrIM.ObjectGet();
                    treeObjects.Nodes.Clear();
                    TreeNode t = new TreeNode();
                    treeObjects.Nodes.Add(t);
                    PopulateProjectTree(this.ActiveProject, t);
                    treeObjects.SelectedNode = t;
                    break;
                case "ProjectRenderEnd":    // 3D rendering of the project has just finished.
                    break;
                case "ProjectFEARun":       // FEA run has been requested
                    break;
                case "ProjectFEAReady":     // the project contains FEA data, OpenBrIM is ready to perform FEA analysis.
                    break;
                case "ProjectFEAStart":     // FEA run has started
                    break;
                case "ProjectFEAComplete":  // FEA run was completed
                    break;
                case "ProjectFEAFailed":    // FEA run failed
                    break;
                case "ObjectVisibilityChanged": // Object visibility has changed, probably user changed visibility of objects through OpenBrIM graphics view
                    break;
                default:                    // likely a custom message from a remote app
                    break;
            }
        }

        void AppendStatusLog(string msg) { listStatusLog.Items.Insert(0, DateTime.Now.ToLongTimeString() + ":" + DateTime.Now.Millisecond.ToString() + ": " + msg); }

        // ask brim object to log the user in brim object will display the login screen.
        private void btnLogin_Click(object sender, EventArgs e) { OpenBrIM.UserLogin(); }

        // ask brim object to log the user out
        private void btnLogout_Click(object sender, EventArgs e) { OpenBrIM.UserLogout(); }

        // you have access to logged in user's first and last name
        private void btnUserInfo_Click(object sender, EventArgs e) { MessageBox.Show("You are currently logged in as " + OpenBrIM.UserFirstName() + " " + OpenBrIM.UserLastName() + "."); }

        void UserLogInEvent(bool isloggedIn)
        {
            if (isloggedIn)
            {
                btnLogin.Enabled = false;
                btnLogout.Enabled = true;
                btnUserInfo.Enabled = true;
                panelOperations.Enabled = true;
                btnConnectedApps.Enabled = true;                
            } 
            else
            {
                btnLogin.Enabled = true;
                btnLogout.Enabled = false;
                btnUserInfo.Enabled = false;
                panelOperations.Enabled = false;
                btnConnectedApps.Enabled = false;
            }
        }

        void PopulateProjectList(List<OBrIMResponseProject> projects)
        {
            projects.Sort((x, y) => x.Name.CompareTo(y.Name));

            // populate the combobox with the project names
            for (int i = 0; i < projects.Count(); i++)
            {
                listProjects.Items.Add(new ListItem(projects[i].Name, projects[i].ID));
            }

            listProjects.SelectedIndex = 0;
        }

        // if user selects a project form the project list
        private void listProjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            treeObjects.Nodes.Clear();
            ListItem item = (ListItem)listProjects.SelectedItem;
            string prjID = item.Value;
            OpenBrIM.ProjectOpen(prjID); // get the objects of the currently open project
        }

        private void PopulateProjectTree(OBrIMObj o, TreeNode t)
        {
            string objID = o.ID;
            string objName = o.Name;
            string objType = o.ObjType;

            for (int i = 0; i < o.Params.Count(); i++ )
            {
                OBrIMParam p = o.Params.Get(i);
                //if (p.Name == "ID") objID = p.Value.AsText();
                //if (p.Name == "N") objName = p.Value.AsText();
                //if (p.Name == "T") objType = p.Value.AsText();
                
                TreeNode pt = new TreeNode();
                pt.Name = p.Parent.ID + "_" + p.Name;
                pt.Text = p.Name + " = " + p.Value.AsText();
                pt.Tag = p.Name;
                t.Nodes.Add(pt);
            }

            t.Text = objName + "(" + objType + ")";
            t.Tag = objID;

            for (int i = 0; i < o.Objects.Count; i++ )
            {
                TreeNode st = new TreeNode();
                t.Name = o.Objects.Get(i).ID;
                PopulateProjectTree(o.Objects.Get(i), st);
                t.Nodes.Add(st);
            }
        }

        // create a new project.
        private void btnNewPrj_Click(object sender, EventArgs e)
        {
            string projectName = "New Project";
            if (Utils.ShowInputDialog("Project Name", ref projectName) == System.Windows.Forms.DialogResult.OK)
            {
                OpenBrIM.ProjectCreate(projectName);
            }
        }

        
        private void treeObjects_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Nodes.Count == 0) 
            {
                // parameter node
                string objectID = (string)e.Node.Parent.Tag; 
                string paramName = (string)e.Node.Tag; 
                OBrIMObj o = this.ActiveProject.GetObject(objectID);
                OBrIMParam p = o.GetParam(paramName);
                this.ActiveParameter = p;
                this.ActiveObject = null;
                this.DisplayParamEditPanel(p);
            }
            else 
            {
                // object node
                string objectID = (string)e.Node.Tag;
                OBrIMObj o = this.ActiveProject.GetObject(objectID);
                this.ActiveParameter = null;
                this.ActiveObject = o;
                this.DisplayParamEditObj(o);
            }
            
        }

        private void DisplayParamEditPanel(OBrIMParam p)
        {
            panelObj.Visible = false;
            panelParam.Visible = true;

            txtParamName.Text = p.Name;
            txtParamDesc.Text = p.Desc;
            
            txtExpression.Text = p.Expr;
            txtValue.Text = p.Value.AsText();

            if (p.Type.ToLower() == "number")
                listParamType.SelectedIndex = 0; // numeric type
            else
                listParamType.SelectedIndex = 1; // text type

            listParamUnitType.SelectedIndex = OBrIMParam.StringToUnitType(p.UType);
            txtUnitCategory.Text = p.UCat;
            
            listRole.SelectedIndex = (p.DisplayRole == "userinput" ? 1 : 0);
            txtUnitCategory.Text = p.DisplayCategory;
            if (p.DisplayWidth == null)
                listDisplayWidth.SelectedIndex = 1;
            else
                listDisplayWidth.SelectedIndex = int.Parse(p.DisplayWidth);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (this.ActiveParameter == null) return;
            OBrIMParam p = this.ActiveParameter;

            p.Name = txtParamName.Text;
            p.Desc = txtParamDesc.Text;

            p.Expr = txtExpression.Text;
            p.Type = (listParamType.SelectedIndex == 0 ? "number" : "text");
            p.UType = OBrIMParam.UnitTypeToString(listParamUnitType.SelectedIndex);

            p.UCat = txtUnitCategory.Text;

            p.DisplayRole = (listRole.SelectedIndex == 1 ? "userinput" : "none");

            p.DisplayCategory = txtUnitCategory.Text;
            p.DisplayWidth = (listDisplayWidth.SelectedIndex == 1 ? null : listDisplayWidth.SelectedIndex.ToString());
        }

        private void btnRevert_Click(object sender, EventArgs e)
        {
            this.DisplayParamEditPanel(this.ActiveParameter);
        }

        private void DisplayParamEditObj(OBrIMObj o)
        {
            panelObj.Visible = true;
            panelParam.Visible = false;
        }

        private void btnDeleteParam_Click(object sender, EventArgs e)
        {
            if (this.ActiveParameter == null) return;
            OBrIMParam p = this.ActiveParameter;
            OpenBrIM.ParamDelete(p.Parent.ID, p.Name);
            foreach (TreeNode n in treeObjects.Nodes.Find(p.Parent.ID + "_" + p.Name, true))
            {
                n.Remove();
            }
        }

        private void btnAddObj_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not yet implemented!");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not yet implemented!");
        }

        private void btnCreateParam_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not yet implemented!");
        }

        private void btnConnectedApps_Click(object sender, EventArgs e)
        {
            // get all apps currently active with user's login
            // if we send true to the method, then we will receive all users currently have the active project open
            OpenBrIM.AppGetConnected(false);
        }

        private void btnDeletePrj_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not yet implemented!");
        }

        private void linkStatusLogClear_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            listStatusLog.Items.Clear();
        }

        private void btnUploadFiles_Click(object sender, EventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.Multiselect = true;
            d.CheckFileExists = true;
            d.ShowDialog();
            if (d.FileNames.Length == 0) return;
            var id = this.ActiveProject.ID;
            for (int i = 0; i < d.FileNames.Length; i++)
            {
                OpenBrIM.FileUpload(d.FileNames[i], id);
            }
        }
    }

    public class ListItem
    {
        public string Name;
        public string Value;
        public ListItem(string name, string value)
        {
            Name = name; Value = value;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}


