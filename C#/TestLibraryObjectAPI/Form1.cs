﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OBrIM;

namespace TestLibraryObjectAPI
{
    public partial class TestLibraryAPI : Form
    {
        private OBrIMConn conn;
        private OBrIMObjList authors;
        private OBrIMObjList objects;
        private string objID;

        public TestLibraryAPI()
        {
            InitializeComponent();
            Enabled = false; // disable the form so user cannot interact with it before login
            conn = new OBrIMConn(UIMode.SeparateWindow, true);
            conn.MessageReceived += Conn_MessageReceived;
        }

        private void Conn_MessageReceived(string eventName, object args)
        {
            if (eventName == "UserLoginSuccess")
            {
                startTimer.Enabled = true;
            }
        }

        private void startTimer_Tick(object sender, EventArgs e)
        {
            startTimer.Enabled = false;
            Enabled = true;
            Populate();
        }

        private void Populate()
        {
            var resp = conn.LibObjList();
            authors = resp.ObjData.Objects;

            listAuthors.Items.Clear();
            for (int i = 0; i < authors.Count; i++)
            {
                listAuthors.Items.Add(authors.Get(i));
            }
            listAuthors.ValueMember = "ID";
            listAuthors.DisplayMember = "Name";
            listAuthors.SelectedIndex = 0;
        }

        private void listAuthors_SelectedIndexChanged(object sender, EventArgs e)
        {
            OBrIMObj author = authors.Get(listAuthors.SelectedIndex);

            objects = author.Objects;

            listObjects.Items.Clear();
            for (int i = 0; i < objects.Count; i++)
            {
                listObjects.Items.Add(objects.Get(i));
            }
            listObjects.ValueMember = "ID";
            listObjects.DisplayMember = "Name";
            listObjects.SelectedIndex = 0;
        }

        private void listObjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            OBrIMObj obj = objects.Get(listObjects.SelectedIndex);
            var resp = conn.LibObjCreate(obj.Name);
            objID = resp.ObjData.ID;

            resp = conn.LibObjGet(objID);
            OBrIMObj o = resp.ObjData;

            if (o == null)
            {
                MessageBox.Show("Could not load the object.");
                return; // this should not happen, right?
            }
            
            listParams.Items.Clear();

            PopulateObjectInputParamList(o);

            listParams.DisplayMember = "Name";
            listParams.ValueMember = "ID";

            if (listParams.Items.Count > 0)
            {
                listParams.SelectedIndex = 0;
            }

            treeCompiled.Nodes.Clear();
            var n = treeCompiled.Nodes.Add("LibObj");
            PopulateCompiledObject(o, n);
        }

        void PopulateObjectInputParamList(OBrIMObj o)
        {
            for (int i=0; i < o.Params.Count(); i++)
            {
                OBrIMParam param = o.Params.Get(i);
                if (param.DisplayRole == "u")
                {
                    listParams.Items.Add(param);
                }
            }

            for(int i=0; i < o.Objects.Count; i++)
            {
                PopulateObjectInputParamList(o.Objects.Get(i));
            }
        }

        void PopulateCompiledObject(OBrIMObj o, TreeNode n)
        {
            
            for (int i = 0; i < o.Params.Count(); i++)
            {
                OBrIMParam param = o.Params.Get(i);
                string value;
                if (param.Ref != "")
                {
                    value = param.Value.AsText();
                }
                else if (param.Value.IsNumber())
                {
                    value = param.Value.AsNumber().ToString();
                }
                else if (param.Value.IsText())
                {
                    value = param.Value.AsText();
                }
                else
                {
                    value = "[listvalues]";
                }

                n.Nodes.Add(o.ID + "_" + param.Name, param.Name + ": " + value);
            }

            for (int i = 0; i < o.Objects.Count; i++)
            {
                OBrIMObj so = o.Objects.Get(i);

                string name = so.ObjType;
                if (so.Name != "")
                {
                    name = name + ": " + so.Name;
                }

                TreeNode sn = n.Nodes.Add(so.ID, name);
                PopulateCompiledObject(so, sn);
            }
        }

        private void listParams_SelectedIndexChanged(object sender, EventArgs e)
        {
            OBrIMParam p = (OBrIMParam)listParams.SelectedItem;
            txtParamValue.Text = p.Expr;
            btnUpdate.Enabled = false;
        }

        private void txtParamValue_KeyDown(object sender, KeyEventArgs e)
        {
            btnUpdate.Enabled = true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            btnUpdate.Enabled = false;
            OBrIMParam p = (OBrIMParam)listParams.SelectedItem;
            conn.LibObjSetParam(objID, p.Name, txtParamValue.Text);

            var resp = conn.LibObjGet(objID);
            OBrIMObj o = resp.ObjData;

            if (o == null)
            {
                MessageBox.Show("Could not load the object.");
                return; // this should not happen, right?
            }

            treeCompiled.Nodes.Clear();
            var n = treeCompiled.Nodes.Add("LibObj");
            PopulateCompiledObject(o, n);
        }
    }
}
