﻿namespace TestLibraryObjectAPI
{
    partial class TestLibraryAPI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listAuthors = new System.Windows.Forms.ListBox();
            this.listObjects = new System.Windows.Forms.ListBox();
            this.listParams = new System.Windows.Forms.ComboBox();
            this.txtParamValue = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.treeCompiled = new System.Windows.Forms.TreeView();
            this.startTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // listAuthors
            // 
            this.listAuthors.FormattingEnabled = true;
            this.listAuthors.Location = new System.Drawing.Point(13, 13);
            this.listAuthors.Name = "listAuthors";
            this.listAuthors.Size = new System.Drawing.Size(179, 303);
            this.listAuthors.TabIndex = 0;
            this.listAuthors.SelectedIndexChanged += new System.EventHandler(this.listAuthors_SelectedIndexChanged);
            // 
            // listObjects
            // 
            this.listObjects.FormattingEnabled = true;
            this.listObjects.Location = new System.Drawing.Point(198, 13);
            this.listObjects.Name = "listObjects";
            this.listObjects.Size = new System.Drawing.Size(179, 303);
            this.listObjects.TabIndex = 1;
            this.listObjects.SelectedIndexChanged += new System.EventHandler(this.listObjects_SelectedIndexChanged);
            // 
            // listParams
            // 
            this.listParams.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listParams.FormattingEnabled = true;
            this.listParams.Location = new System.Drawing.Point(384, 13);
            this.listParams.Name = "listParams";
            this.listParams.Size = new System.Drawing.Size(152, 21);
            this.listParams.TabIndex = 2;
            this.listParams.SelectedIndexChanged += new System.EventHandler(this.listParams_SelectedIndexChanged);
            // 
            // txtParamValue
            // 
            this.txtParamValue.Location = new System.Drawing.Point(543, 14);
            this.txtParamValue.Name = "txtParamValue";
            this.txtParamValue.Size = new System.Drawing.Size(120, 20);
            this.txtParamValue.TabIndex = 3;
            this.txtParamValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtParamValue_KeyDown);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Location = new System.Drawing.Point(669, 12);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 4;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // treeCompiled
            // 
            this.treeCompiled.Location = new System.Drawing.Point(384, 41);
            this.treeCompiled.Name = "treeCompiled";
            this.treeCompiled.Size = new System.Drawing.Size(360, 275);
            this.treeCompiled.TabIndex = 5;
            // 
            // startTimer
            // 
            this.startTimer.Interval = 3000;
            this.startTimer.Tick += new System.EventHandler(this.startTimer_Tick);
            // 
            // TestLibraryAPI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 327);
            this.Controls.Add(this.treeCompiled);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.txtParamValue);
            this.Controls.Add(this.listParams);
            this.Controls.Add(this.listObjects);
            this.Controls.Add(this.listAuthors);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "TestLibraryAPI";
            this.Text = "OpenBrIM Library API Sample...";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listAuthors;
        private System.Windows.Forms.ListBox listObjects;
        private System.Windows.Forms.ComboBox listParams;
        private System.Windows.Forms.TextBox txtParamValue;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TreeView treeCompiled;
        private System.Windows.Forms.Timer startTimer;
    }
}

