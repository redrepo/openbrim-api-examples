﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridMeshTest
{
    class Program
    {
        static OBrIM.OBrIMConn conn;

        static void Main(string[] args)
        {
            System.Console.WriteLine("Event: Loading... Please wait... ");

            conn = new OBrIM.OBrIMConn(OBrIM.UIMode.SeparateWindow);
            conn.MessageReceived += MessageReceived;   
        }

        private static void MessageReceived(string eventName, object args)
        {
            System.Console.WriteLine("Event: " + eventName);

            if (eventName == "AppLoaded")
            {
                OBrIM.Tools.GridMesh gMesh = new OBrIM.Tools.GridMesh(conn);

                OBrIM.Tools.Polygon poly = new OBrIM.Tools.Polygon(false, 1);
                poly.points.Add(new OBrIM.Tools.PolygonPoint(0, 0));
                poly.points.Add(new OBrIM.Tools.PolygonPoint(10, 0));
                poly.points.Add(new OBrIM.Tools.PolygonPoint(10, 10));
                poly.points.Add(new OBrIM.Tools.PolygonPoint(0, 10));
                gMesh.Polygons.Add(poly);

                OBrIM.Tools.GridMeshOutput mesh = gMesh.Mesh(10, 10);

                System.Console.WriteLine("Points");
                System.Console.WriteLine("======");
                for (var i = 0; i < mesh.points.Count; i++)
                {
                    System.Console.WriteLine("Point " + i.ToString() + ": " + mesh.points[i].x.ToString() + ", " + mesh.points[i].x.ToString());
                }

                System.Console.WriteLine("Quads");
                System.Console.WriteLine("======");
                for (var i = 0; i < mesh.quads.Count; i++)
                {
                    System.Console.WriteLine("Quad " + i.ToString() + ": " + mesh.quads[i].points[0].ToString() + ", " + mesh.quads[i].points[1].ToString() + ", " + mesh.quads[i].points[2].ToString() + ", " + mesh.quads[i].points[3].ToString());
                }

                System.Console.WriteLine("End Of Data");

                System.Console.WriteLine("Press any key to close...");
                Console.ReadKey();
                conn.Exit();
            }
        }
    }
}
