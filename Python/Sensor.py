from urllib.request import Request, urlopen  # Python 3
import urllib
import os.path
import time 
import random


# make sure to replace the data type with the data type created in your project
# you can get this data type from within OpenBrIM App
# From the main app menu, goto Health View
# You will see the data type ID on Settings tab. 
datatype = "06cb6d6a-2198-4a09-a95d-45688e352a60" # API Data Type ID


# email and authentication token (NOT PASSWORD) 
# are always required with each request
email = ""
auth = "" # authentication token

# we will use this function to make requests to the server
def RemoteRequest(action, values):
    DELIMETER = "._DEL_."
    
    # we will use /connect service
    # each request must have action, email and authentication token
    url = "http://openbrim.org/connect?";
    values["action"] = action
    values["email"] = email
    values["password"] = auth
    
    url = url + urllib.parse.urlencode(values)
    q = Request(url)
    content = urlopen(q).read().decode("utf-8") 
    
    resp = content.split(DELIMETER)
    return resp;


# 0- authentication
#================================
print("\n\nStep 0: Authentication")
print("=============================================")


# let see if we have login information in a file.
# if the auth token in the file is still valid, we don't 
# need to authenticate again
authSuccess = False
authFile = "auth.ini"
if os.path.isfile(authFile):
    f = open(authFile)
    lines = f.readlines()
    email = lines[0]
    auth = lines[1]
    f.close()
    
    print (auth)
    # let's check if this auth token is still valid
    if RemoteRequest("loginvalidate", { })[0] == "OK":
        authSuccess = True
        
while not authSuccess:
    email = input("Email: ")
    auth = input("Password: ")
    resp = RemoteRequest("login", { })
    
    # user account requires 2 factor authentication?
    if resp[0] == "SMSVer": 
        smstoken = resp[1]
        phone = resp[2]
        smscode = input("SMS Verification Code (sent to " + phone + "): ")
        resp = RemoteRequest("login", { "smsver": smstoken, "smscode": smscode})
    
    # was auth success?
    if resp[0] == "OK":
        authSuccess = True
        auth = resp[2] # we will use this token for the rest of the requests
        
        # this auth is good for 30 days, let's hold on to it in a file for later runs
        f = open(authFile, "w")
        f.write(email + "\n" + auth)
        f.close()
    else: # probably login failed
        print(resp[0])

            
# we have authentication, we can now send sensor data to 
#================================
print("\n\nStep 1: Sending Sensor Data")
print("=============================================")
for num in range(0,50):
    sensor = "SENSOR1"
    dtime = int(round(time.time() * 1000)) # current time in miliseconds since 1970
    resp = RemoteRequest("bigdataadd", { 
        "type": datatype, 
        "data": str(dtime) + "," + sensor + "," + str(random.random()*100) })
    print (resp[0])
    if not resp[0] == "OK":
        break
    time.sleep(0.1)  # Delay for 1 second