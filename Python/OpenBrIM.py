from urllib.request import Request, urlopen  # Python 3
import urllib
import os.path

# email and authentication token (NOT PASSWORD) 
# are always required with each request
email = ""
auth = "" # authentication token

# we will use this function to make requests to the server
def RemoteRequest(action, values):
    DELIMETER = "._DEL_."
    
    # we will use /connect service
    # each request must have action, email and authentication token
    url = "https://openbrim.org/connect?";
    values["action"] = action
    values["email"] = email
    values["password"] = auth
    
    url = url + urllib.parse.urlencode(values)
    q = Request(url)
    content = urlopen(q).read().decode("utf-8") 
    
    resp = content.split(DELIMETER)
    return resp;


# 0- authentication
#================================
print("\n\nStep 0: Authentication")
print("=============================================")


# let see if we have login information in a file.
# if the auth token in the file is still valid, we don't 
# need to authenticate again
authSuccess = False
authFile = "auth.ini"
if os.path.isfile(authFile):
    f = open(authFile)
    lines = f.readlines()
    email = lines[0]
    auth = lines[1]
    f.close()
    
    print (auth)
    # let's check if this auth token is still valid
    if RemoteRequest("loginvalidate", { })[0] == "OK":
        authSuccess = True
        
while not authSuccess:
    email = input("Email: ")
    auth = input("Password: ")
    resp = RemoteRequest("login", { })
    
    # user account requires 2 factor authentication?
    if resp[0] == "SMSVer": 
        smstoken = resp[1]
        phone = resp[2]
        smscode = input("SMS Verification Code (sent to " + phone + "): ")
        resp = RemoteRequest("login", { "smsver": smstoken, "smscode": smscode})
    
    # was auth success?
    if resp[0] == "OK":
        authSuccess = True
        auth = resp[2] # we will use this token for the rest of the requests
        
        # this auth is good for 30 days, let's hold on to it in a file for later runs
        f = open(authFile, "w")
        f.write(email + "\n" + auth)
        f.close()
    else: # probably login failed
        print(resp[0])

            
# we have authentication, we can now interact with OpenBrIM backend 

# 1- create a new project
#================================
print("\n\nStep 1: Create a new project")
print("=============================================")
projectTitle = input("New Project Title: ")
resp = RemoteRequest("create", { "projectname": projectTitle })

if not resp[0] == "OK":
    print("Creating project failed: " + resp[0])
    quit()
    
projectID = resp[1]

# 2- update project data
#================================
print("\n\nStep 2: update project data")
print("=============================================")

data = "<O T=\"Project\" N=\"" + projectTitle + "\" ID=\"" + projectID + "\" >"
data = data + "<P N=\"TestP\" V=\"10\"/>"
data = data + "</O>"

resp = RemoteRequest("save", { "project": projectID, "data": data })

if resp[0] == "OK":
    print("Project updated successfully.")
else:
    print("Updating project failed: " + resp[0])
    quit()
    
# 3- retrieve all my projects
#================================            
print("\n\nStep 3: retrieve all my projects")
print("=============================================")

resp = RemoteRequest("list", { })

if resp[0] == "OK":
    print("Project list retrieved successfully.")
else:
    print("Getting list of projects failed: " + resp[0])
    quit()  
    
for pdata in resp:
    if pdata == "" or pdata == "OK":
        continue;

    prj = pdata.split(".__.");

    prjID = prj[0]
    prjTitle = prj[1]
    prjMod = prj[2] # last modified date EPOC 
    
    print(prjID + ": " + prjTitle + " - " + prjMod)
    

# 4- retrieve data of a particular project
#================================            
print("\n\nStep 4: retrieve data of a particular project")
print("=============================================")

resp = RemoteRequest("get", { "project": projectID })

if resp[0] == "OK":
     print("Project retrieved successfully.")
else:
    print("Getting project data failed: " + resp[0])
    quit()  
    
projectdata = resp[3];
print (projectdata)
            
            
            
            
            